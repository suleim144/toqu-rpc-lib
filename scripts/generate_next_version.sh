#!/bin/sh

RE='[^0-9]*\([0-9]*\)[.]\([0-9]*\)[.]\([0-9]*\)\([0-9A-Za-z-]*\)'

MAJOR_VERSION=`git tag -l --sort=-version:refname | head -1 | sed -e "s#$RE#\1#"`
LATEST_TAG=`git tag -l | grep "^v$MAJOR_VERSION" | sort -rV | head -1`
PATCH_VERSION=0

if [ -z "$LATEST_TAG" ]; then
    MAJOR_VERSION=0
    MINOR_VERSION=0
    PATCH_VERSION=1
else
    MINOR_VERSION=$(echo $LATEST_TAG | cut -f2 -d'.')
    MINOR_VERSION=$((MINOR_VERSION + 1))
fi

VERSION="v${MAJOR_VERSION}.${MINOR_VERSION}.${PATCH_VERSION}"

echo $VERSION