#!/usr/bin/env bash
status=0
for package in $(go list ../... | grep -v -e vendor -e mocks); do
 go vet $package; 
 if [[ $? -ne 0 && $status -eq 0 ]];then
    status=1
 fi
done
exit $status